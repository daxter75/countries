import { createApp } from "vue";
import App from "./App.vue";
import router from "./router";
import mixins from "./mixins";
import "./assets/tailwind.css";

createApp(App).mixin(mixins).use(router).mount("#app");
