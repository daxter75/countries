import { createRouter, createWebHistory } from "vue-router";
import Home from "../views/Home.vue";

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
  },
  {
    path: "/about",
    name: "About",
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/About.vue"),
  },
  {
    path: "/region/:slug",
    name: "Region",
    props: true,
    component: () => import(/* webpackChunkName: "region" */ "../views/Region"),
  },
  {
    path: "/country/:countryCode",
    name: "CountryDetails",
    props: true,
    component: () =>
      import(
        /* webpackChunkName: "countrydetails" */ "../views/CountryDetails"
      ),
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
