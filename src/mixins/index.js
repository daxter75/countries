const funcs = {
  methods: {
    formatNumber(num) {
      return num.toString().replace(/(.)(?=(\d{3})+$)/g,'$1,')
    },
  },
};

export default funcs;
